<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_user = \App\Models\MyUser::all();
        return view('user.index', ['data_user' => $data_user]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        try{
            \App\Models\MyUser::create($request->all());
            return redirect('/users')->with('success', 'Success adding new account!');
        }catch(Exception $exc){
            return redirect('/users')->with('failed', 'Your email is already, choose edit if you want change something');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Models\MyUser::find($id);
        return view('user/edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $user = \App\Models\MyUser::find($id);
            $user->update($request->all());
            return redirect('/users')->with('success', 'Success editing account!');
        }catch(Exception $exc){
            return redirect('/users')->with('failed', 'Please, check your email!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = \App\Models\MyUser::find($id);
        $user->delete($user);
        return redirect('/users')->with('success', 'Success deleting account!');
    }
}
