@extends('layouts.default')
@section('content')

    <a href="/" style="color:black; text-decoration: none;"><h5 style="padding: 30px 0px  0px 30px;">Management Accounts</h5></a>
    <div class="container">
    @if(session('success'))
        <div class="alert alert-success" role="alert">
        {{ session('success') }}
        </div>
    @endif 
    <!-- Button trigger modal -->
<div>
<button style="margin-bottom: 30px" type="button" class="btn btn-primary float-lg-right" data-toggle="modal" data-target="#exampleModal">
  Create new account
</button>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create new account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

<div class="modal-body">
    <form action="/users/create" method="POST">
    {{csrf_field()}}
    <div class="form-group">
    <label for="name">Name</label>
    <input name="name" type="text" class="form-control" id="name" placeholder="Enter your name">
  </div>
  <div class="form-group">
    <label for="username">Username</label>
    <input name="username" type="text" class="form-control" id="username"  placeholder="Enter your username">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" a placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Role</label>
    <select name="role_id" class="form-control" id="exampleFormControlSelect1">
      <option value="2">User</option>
      <option value="1">Admin</option>
    </select>
  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
</form>
            <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Role</th>
                <th scope="col">Name</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <!-- <th scope="col">Password</th> -->
                 <!-- saya rasa password seharusnya tidak ditampilkan  -->
                <th scope="col">Created At</th>
                <th scope="col">Updated At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data_user as $key => $user)
        
            <td>{{$key + 1}}</td>
            @if($user->role_id == "2")
            <td>User</td>
            @else 
            <td>Admin</td>
            @endif
            <td>{{$user -> name}}</td>
            <td>{{$user -> username}}</td>
            <td>{{$user -> email}}</td>
            <!-- <td>{{$user -> password}}</td> -->
            <!-- saya rasa password seharusnya tidak ditampilkan  -->
            <td>{{$user -> created_at}}</td>
            <td>{{$user -> updated_at}}</td>
            <td><a href="/users/{{$user->id}}/edit" class="btn btn-warning btn-sm">Edit</a> </td>
            <td><a href="/users/{{$user->id}}/delete" onclick="return confirm('Are you sure deleting ' + '{{$user -> username}}' + '\'s account?')" class="btn btn-danger btn-sm">Delete</a> </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    <script>
    function goBack() {window.history.back();}
    </script>
@endsection