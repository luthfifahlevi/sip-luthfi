@extends('layouts.default')
@section('content')

<h1 style="padding: 30px 0px  0px 30px">Editing Account</h1>
    
<div class="container">
    @if(session('success'))
        <div class="alert alert-success" role="alert">
        {{ session('success') }}
        </div>
    @endif 
    <!-- Button trigger modal -->

<form action="/users/{{$user->id}}/update" method="POST">
    {{csrf_field()}}
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" value="{{$user->name}}" type="text" class="form-control" id="name" placeholder="Enter your name">
    </div>
    <div class="form-group">
        <label for="username">Username</label>
        <input name="username" value="{{$user->username}}" type="text" class="form-control" id="username"  placeholder="Enter your username">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input name="email" value="{{$user->email}}" type="email" class="form-control" id="exampleInputEmail1" a placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
     </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Role</label>
        <select name="role_id" class="form-control" id="exampleFormControlSelect1">
            <option value="2" @if($user->role_id == "2") selected @endif>User</option>
            <option value="1" @if($user->role_id == "1") selected @endif>Admin</option>
        </select>
    </div> 
    <button type="button" onclick="goBack()" class="btn btn-secondary">Cancel</button>
    <button type="submit" class="btn btn-warning">Update</button>
</form>
</div>

<script>
    function goBack() {window.history.back();}
</script>
@endsection