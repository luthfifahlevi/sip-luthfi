<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'MyUsersController@index');
Route::post('/users/create', 'MyUsersController@create');
Route::get('/users/{id}/edit', 'MyUsersController@edit');
Route::post('/users/{id}/update', 'MyUsersController@update');
Route::get('/users/{id}/delete', 'MyUsersController@delete');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
