<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\MyUser;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //untuk percobaan
        MyUser::create([
            'role_id' => 1,
            'name'	=> 'Administrator',
            'username' => 'admin',
            'email' => 'admin@trying.com',
            'password'	=> bcrypt('secret'),
        ]);

        //dump-user
        for($i=1;$i<11;$i++){
            MyUser::create([
                'role_id' => rand(1,2),
                //1 for Admin, 2 for User, Random
                'name'	=> 'User' . $i ,
                'username' => 'Username' . $i,
                'email' => 'UserEmail' . $i . '@trying.com',
                'password'	=> bcrypt('secret'),
            ]);
        }
    }
}
